# -*- coding: utf-8 -*-
"""
Created on Wed Apr  7 22:23:03 2021

@author: Guenole CHEROT
"""

import pandapower as pp
import pandapower.networks
import warnings
warnings.filterwarnings("ignore")

from rt_congestion_control.so import SO
from rt_congestion_control.communication import Communication
from rt_congestion_control.admm import Admm

import os
absolute_dirpath = os.path.abspath(os.path.dirname(__file__))

########################################################################################################
########################################### Initialisation #############################################
########################################################################################################

### Import data
# Import network
panda_network = pp.from_pickle(os.path.join(absolute_dirpath,"../../test_case/use/case_CIGRE_lv_UMASS.p"))
n_agent = len(panda_network.flex_data)

### Init classes
So = SO(n_agent,
        grid_cost=0,
        strategy="PI",
        Kp=0.006,
        Ki=0.005,
        Kd=0,
        line_limits=panda_network.line["max_loading_percent"][0],
        grid_cost_type = "uniq")
communication = Communication(n_agent, grid_cost=So.grid_cost)
ADMM = Admm(panda_network, So, communication, rho=700, compute_opf=2)

### Run
ADMM.run(warm_start=True, max_it=100, plot=False)



########################################################################################################
################################################ Save ##################################################
########################################################################################################

import pickle
filehandler = open(os.path.join(absolute_dirpath,"results_UMASS.obj"), 'wb') 
pickle.dump(ADMM, filehandler)

