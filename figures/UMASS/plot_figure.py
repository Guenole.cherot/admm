# -*- coding: utf-8 -*-
"""
Created on Mon Apr 26 10:39:36 2021

@author: Guenole
"""
import numpy as np

import pickle 

import os
absolute_dirpath = os.path.abspath(os.path.dirname(__file__))

########################################################################################################
############################################ Import files ##############################################
########################################################################################################

# Import main results
file = open(os.path.join(absolute_dirpath,'results_UMASS.obj'), 'rb') 
ADMM = pickle.load(file)

########################################################################################################
################################################ PLOT ##################################################
########################################################################################################
ADMM.memory.plot_fig1(5,4)
ADMM.memory.plot_convergence(scale = True)
ADMM.memory.plot_cost_vs_flex(5,4)
ADMM.memory.plot_overload_histogram()