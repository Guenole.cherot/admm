# -*- coding: utf-8 -*-
"""
Created on Wed Apr  7 22:23:03 2021

@author: Guenole CHEROT
"""

import pandapower as pp
import pandapower.networks
import warnings
warnings.filterwarnings("ignore")

from rt_congestion_control.so import SO
from rt_congestion_control.communication import Communication
from rt_congestion_control.admm import Admm

import os
absolute_dirpath = os.path.abspath(os.path.dirname(__file__))

########################################################################################################
########################################### Initialisation #############################################
########################################################################################################

### Import data
# Import network
panda_network = pp.from_pickle(os.path.join(absolute_dirpath,"../../test_case/use/case_CIGRE_lv_pecan.p"))
# Change line limits
panda_network.line.max_i_ka *= 0.5
# Set max loading percent
panda_network.line["max_loading_percent"] = 100

### Simulation : set the time interval of the simulation
begin = 1000
end = 1250
panda_network.data = panda_network.data[panda_network.data["time"]<end]
panda_network.data = panda_network.data[panda_network.data["time"]>=begin]
panda_network.data.time -= begin # Index has to strat at 0

### Set agent
# Ext_grid fleibility
panda_network.flex_data.flexibility[0] = 10
# Agent flexibility so that the grid price is around something usual (20€/MWh)
panda_network.flex_data.flexibility *= 0.01
# Number of agent, ext grid is counted as an agent
n_agent = len(panda_network.flex_data)

### Init classes
So = SO(n_agent,
        grid_cost=0,
        strategy="PI",
        Kp=0.01,
        Ki=0.01,
        Kd=0,
        line_limits=panda_network.line["max_loading_percent"][0],
        grid_cost_type = "uniq")
communication = Communication(n_agent, grid_cost=So.grid_cost)
ADMM = Admm(panda_network, So, communication, rho=700, compute_opf=2)

### Run
ADMM.run(warm_start=True, max_it=100, plot=False)



########################################################################################################
################################################ Save ##################################################
########################################################################################################

import pickle
filehandler = open(os.path.join(absolute_dirpath,"results_ISGT.obj"), 'wb') 
pickle.dump(ADMM, filehandler)

