# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 11:24:27 2020

@author: Guenole CHEROT
"""
from rt_congestion_control.agent import Agent
import pandas as pd


def init():
    time_series = {'dataid':[56911,56911,56911,56911,56911],
                   'localminute':['2018-06-01 16:41:00-05','2018-06-01 16:42:00-05','2018-06-01 16:43:00-05',
                                  '2018-06-01 16:44:00-05','2018-06-01 16:45:00-05'],
                    'grid':[0,1,2,2,4],
                    'time':[0,1,2,3,4]}
    time_series = pd.DataFrame(data=time_series)
    
    ag = Agent(name = "test",
               ID = 0,
               element = 0,
               et = "load",
               bus = 3,
               n_agent = 5,
               zone = 0,
               flexibility=10,
               grid_cost_type = "uniq",
               rho=100,
               partners="all",
               time_series=time_series)
    return ag

def test_init():
    ag = init()
    assert ag.name == "test"
    assert ag.ID == 0
    assert ag.element == 0
    assert ag.et == "load"
    assert ag.bus == 3
    assert ag.n_agent == 5
    assert ag.zone == 0
    assert ag.flex == 10
    assert ag.grid_cost_type == "uniq"
    assert ag.rho == 100
    assert (ag.partners == [0, 1, 2, 3, 4]).all()
    assert ag.t == 0

def test_n_simu():
    ag = init()
    n = ag.n_simu()
    assert n == 5

def test_print():
    ag = init()
    try :
        print(ag)
        ag.plot_power_trade()
        ag.plot_bounds()
        ag.plot_gamma()
        ag.plot_cost()
    except :
        assert False, "One of the plot did not work"
    